<?php

require_once dirname(__FILE__) . '/../vendor/autoload.php';

 $site_url = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
 if ($_SERVER["SERVER_PORT"] == 80 || $_SERVER["SERVER_PORT"] == 443){
    $site_url .= $_SERVER["SERVER_NAME"];
 } else {
    $site_url .= $_SERVER["SERVER_NAME"] . ':' . $_SERVER["SERVER_PORT"];
 }
 define('WP_URI', $site_url);
 define( 'WP_SITEURL', WP_URI . '/wp' );
 define( 'WP_HOME', WP_URI );
 define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content' );
 define( 'WP_CONTENT_URL', WP_URI . '/wp-content' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', getenv('DB_NAME'));

/** MySQL database username */
define('DB_USER', getenv('DB_USER'));

/** MySQL database password */
define('DB_PASSWORD', getenv('DB_PASSWORD'));

/** MySQL hostname */
define('DB_HOST', getenv('DB_HOST'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '*>Ve[-0T-7;.#*&aMpTlCR(uwp7yRJwGk:TBe<Nct1$3I+pkwLAC>YE|][@%|He.');
 define('SECURE_AUTH_KEY',  'y?+Q;+)7uHyh-5HO2fBUL}SEkVRKy0LezK@i*M6vaCQ[#0C&kaaBQ({0LE33C)`[');
 define('LOGGED_IN_KEY',    'hB}RcM;y$|E8uc<~w5bT5Yd8(;N0$p|NG{O${c5AC~2|?E;D_,6,?@]y9UCVEbE]');
 define('NONCE_KEY',        '?Jt4`oS3Cu~:~[4ZdaK*%T+?!<$@|VQ+;wkORNxz~4~|PBAI-+6J/UAW?@`?sK>b');
 define('AUTH_SALT',        'n[8HQaC?xA4$S0!OXCLVm-,z7e<pu-JJY$I1OZz|TCF)X0/PeIamJJRiJ)Bx?tmQ');
 define('SECURE_AUTH_SALT', 'dY@ok4n[g@%47;o)P%-_@z3/reFiO4cEw(S@0t@6OZTkegI-Wz-`}Gf-}dm=R^Tn');
 define('LOGGED_IN_SALT',   '@(K`N`chi+wHa<5d[Y#oQ1Md4fVh-TO-Q%|xRr-$a;m-M-/+`)&E!$X-+ufbU*=+');
 define('NONCE_SALT',       '~W#MZQNdc@d;[*]B!Nxes+i,rFEYpb`Cf;7:fIOj0Y[E[vACw?X|P?-FfMl!e1+{');
//store keys in htaccess, contentform 7

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
